﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using JTSMVC.Models;

namespace JTSMVC.Controllers
{
    public class MovementsController : Controller
    {
        //
        // GET: /Movements/

        public ActionResult Index(string itemNumber)
        {
            Entities1 dbContext = new Entities1();
            string sql = @"SELECT  tblEstilosFacturados.Numero_factura AS Invoice, 
                            tblFacturasEmbarque.Numero_guia as GuideNumber, convert(varchar, tblEstilosFacturados.Fecha_factura, 101)
                            AS Date, tblEstilosFacturados.Estilo as Item, tblEstilosFacturados.Kilate as Carat, 
                            tblEstilosFacturados.Orden as Orders, tblEstilosFacturados.Cantidad_piezas 
                            AS Quantity, Round(tblEstilosFacturados.Precio_labor,2) AS Labor, 
                            tblEstilosFacturados.weigth_Neto AS NetWeight, Round(tblEstilosFacturados.weigth_Neto / tblEstilosFacturados.Cantidad_piezas,2) AS AverageWeight, 
                            tblEstilosFacturados.Serie as Series, tblItemCategories.CatName AS Category, 
                            tblEstilosFacturados.Caja as Box, tblEstilosFacturados.Bag 
                            FROM ((tblFacturasEmbarque INNER JOIN 
                            tblEstilosFacturados ON tblFacturasEmbarque.IdFactura = tblEstilosFacturados.FactId) 
                            LEFT OUTER JOIN (tblItems LEFT OUTER JOIN tblItemCategories ON tblItems.CatId = tblItemCategories.CatId) 
                            ON tblEstilosFacturados.ItemId = tblItems.ItemID) 
                            WHERE (tblEstilosFacturados.Estilo = @ItemId) 
                            ORDER BY tblEstilosFacturados.Fecha_factura DESC, tblEstilosFacturados.Serie, tblEstilosFacturados.Kilate";
            int? itemId = dbContext.tblItems.Where(it => it.ItemNumber == itemNumber).Select(it => it.ItemID).FirstOrDefault();
            List<Movements> movements = new List<Movements>();
            if (itemId != null && itemId > 0)
            {
                SqlParameter param = new SqlParameter("@ItemId",itemNumber);
                var query = dbContext.Database.SqlQuery<Movements>(sql, param);
                if (query != null)
                {
                    foreach (Movements item in query)
                    {
                        movements.Add(item);
                    }
                }
            }

            return View(movements);
        }

        public class Movements
        {
            public Movements()
            {

            }

            public int? Invoice { get; set; }
            public string GuideNumber { get; set; }
            public string Date { get; set; }
            public string Item { get; set; }
            public string Carat { get; set; }
            public string Orders { get; set; }
            public double Quantity { get; set; }
            public decimal Labor { get; set; }
            public double NetWeight { get; set; }
            public double AverageWeight { get; set; }
            public string Series { get; set; }
            public string Category { get; set; }
            public int Box { get; set; }
            public Int16 Bag { get; set; }
        }
    }
}
