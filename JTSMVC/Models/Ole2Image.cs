﻿using System;
using System.Text;
using System.IO;
using System.Data.OleDb;
using System.Data.SqlClient;

namespace JTSMVC.Models
{
    public class OLE2IMG
    {

        public OLE2IMG()
        {

        }
      
        public MemoryStream Ole2Img(object sourcCmd)
        {
            short sFlag = 0;
            int nIndex = 0;
            int nCount = 0;
            int nOffset = 0;
            int nImgLen = 0;
            int nReadbyte = 14400;
            string szImgType = string.Empty;

            try
            {
                byte[] bData = (byte[])sourcCmd; //cmdQuery.ExecuteScalar();
                if (bData.Length > 0)
                {
                    MemoryStream memStream = new MemoryStream(bData, true);
                    byte[] bArray = new byte[nReadbyte];
                    nCount = memStream.Read(bArray, 0, nReadbyte);

                    if (bArray[78] == (byte)0x42 && bArray[79] == (byte)0x4D) //BMP FORMAT
                    {
                        sFlag = 1;
                        nOffset = 78;
                        szImgType = "image/bmp";
                    }
                    else
                    {
                        for (nIndex = 78; nIndex < nReadbyte - 2; nIndex++)
                        {
                            if (bArray[nIndex] == (byte)0xFF && bArray[nIndex + 1] == (byte)0xD8) //JPG FORMAT
                            {
                                sFlag = 2;
                                nOffset = nIndex;
                                szImgType = "image/pjpeg";
                                break;
                            }
                            else if (bArray[nIndex] == (byte)0x25 && bArray[nIndex + 1] == (byte)0x50) //PDF FORMAT
                            {
                                sFlag = 3;
                                nOffset = nIndex;
                                szImgType = "application/pdf";
                                break;
                            }
                            else if (bArray[nIndex] == (byte)0xD0 && bArray[nIndex + 1] == (byte)0xCF) //MSWORD FORMAT
                            {
                                sFlag = 4;
                                nOffset = nIndex;
                                szImgType = "application/msword";
                                break;
                            }
                        }
                    }

                    if (sFlag == 1 || sFlag == 2)
                    {
                        nImgLen = bData.Length - nOffset;
                        memStream.Position = 0;
                        memStream.Write(bData, nOffset, nImgLen);
                        memStream.Position = 0;
                        byte[] bImgData = new byte[nImgLen];
                        nCount = memStream.Read(bImgData, 0, nImgLen);
                        OleDbParameter paraImg = new OleDbParameter("@Image", OleDbType.LongVarBinary, nImgLen);
                        paraImg.Value = bImgData;
                        return memStream;
                    }
                }

            }//end try
            catch (Exception ex)
            {

            }

            return null;
        }

    }
}