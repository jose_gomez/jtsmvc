﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Web;
using System.Data.SqlClient;

namespace JTSMVC.Models
{
    public class InventoryCreditsUtility
    {
        Entities1 dbContext = new Entities1();
        public string GetNextActivityInOut(int travelerId)
        {
//            string query = @"SELECT tblJobTracking.ActivityId, tblJobTracking.NextActivityId,
//                            tblJobTracking.EntryType FROM tblJobTracking Inner join tblTravelerHeaders 
//                            on tblJobTracking.TravelerID = tblTRavelerHeaders.TRavelerID 
//                            WHERE tblTravelerHeaders.TravelerId = @TravelerId  AND 
//                            tblJobTracking.EntryId = tblTravelerHeaders.LatestScanId ";
//            CommonDataManager manager = new CommonDataManager(query);

            var query = (from jt in dbContext.tblJobTrackings join th in dbContext.tblTravelerHeaders
                                on jt.TravelerID equals th.TravelerId where th.TravelerId == travelerId && jt.EntryID == th.LatestScanId
                                select new {jt.ActivityID, jt.NextActivityID, jt.EntryType}).ToList();

            string activityName = "";
            bool nextActivityExists = false;
            int? activityId = 0;
            int? nextActivityId = 0;
            int? entryType = 0;
            string activityInOut = "";


            if (query.Count > 0)
            {
                foreach (var item in query)
                {
                    activityId = item.ActivityID;
                    nextActivityId = item.NextActivityID;
                    entryType = item.EntryType;
                }

                if (nextActivityId == -1)
                {
                    nextActivityId = activityId;
                }
                else
                {
                    nextActivityExists = true;
                }

                activityName =  dbContext.tblActivities.Where(a => a.ActivityId == nextActivityId).Select(a => a.ActivityName).FirstOrDefault();
            }
            else
            {
                activityName = "Release";
            }

            switch (entryType)
            {
                case 1:
                case 3:
                case 5:
                case 6:
                case 7:
                    {
                        if (nextActivityExists)
                        {
                            activityInOut = "Entry - " + activityName;
                        }
                        else
                        {
                            activityInOut = "Undefined";
                        }
                    }
                    break;
                case 2:
                case 4:
                    {
                        activityInOut = "Exit - " + activityName;
                    }
                    break;
                default:
                    activityInOut = activityName;
                    break;
            }

            switch (activityInOut.ToLower())
            {
                case "exit - polish":
                case "exit - casting":
                case "exit - cut & grind":
                case "exit - diamond Cut":
                case "exit - ensamble":
                case "exit - tru-kay":
                case "exit - assembling":
                case "exit - cadena":
                    {
                        activityInOut = this.get_nextTravelerInOut(travelerId);//JTSSqlServerRepository.get_nextTravelerInOut(travelerId);
                    }
                    break;
            }

            return activityInOut;
        }

         public  string get_nextTravelerInOut(int travelerId)
        {
            string query1 = @"Select tblSubPasos.SubActivityId, EntryType
                            From tblSubPasos Where TravelerID = @travelerid";
            string query3 = @"Select Name from tblSubActivities Where
                            SubActId = @subAct";
            var query =  (from sub in dbContext.tblSubPasos where sub.TravelerID == travelerId select new {sub.SubActivityID, sub.EntryType}).ToList();
            

            bool nextActivityExists = false;
            string result = "";
            int? actId = 0;
            int? entryType = 0;

//DataTable dt = manager.GetDataTable();

            if (query.Count > 0)
            {
                foreach (var item in query)
                {
                    actId = item.SubActivityID;// int.Parse(row["SubActivityId"].ToString());
                    entryType = item.EntryType;// int.Parse(row["EntryType"].ToString());
                }
                if (actId != -1)
                {
                    nextActivityExists = true;
                }
                var query2 = (from sub in dbContext.tblSubActivities where sub.SubActID == actId select sub).ToList();
                //manager.ClearParameters();
                //manager.SetCommandText(query2);
                //manager.AddWithValue("@subAct", actId);
                //dt = manager.GetDataTable();

                if (query2.Count > 0)
                {
                    foreach (var item in query2)
                    {
                        result = item.Name;//  name row["name"].ToString();
                    }
                }
                else
                {
                    result = "?";
                }
            }
            else
            {
                result = "Exit - Polish";
            }

            switch (entryType)
            {
                case 1:
                case 3:
                case 5:
                case 6:
                case 7:
                    {
                        if (nextActivityExists)
                        {
                            result = "Entry - " + result;
                        }
                        else
                        {
                            result = "Undefined";
                        }
                    }
                    break;
                case 2:
                    {
                        result = "Exit - " + result;
                    }
                    break;
                case 4:
                    {
                        result = "Exit - " + result;
                    }
                    break;
            }

            return result;
        }

         public  DateTime GetDateCreated(int travelerId)
         {
             string query1 = @"SELECT tblTravelerHeaders.IsSplit, tblTravelerHeaders.SplitDateTime, 
                            tblJobStatus.IsStarted, tblJobStatus.DateStarted 
                            FROM tblTravelerHeaders INNER JOIN tblJobStatus 
                            ON tblTravelerHeaders.JobID = tblJobStatus.JobId 
                            WHERE tblTravelerHeaders.TravelerID = @TravelerId";
             var query = (from th in dbContext.tblTravelerHeaders
                          join jt in dbContext.tblJobStatus
                              on th.JobID equals jt.JobId
                          where th.TravelerId == travelerId
                          select new { th.IsSplit, th.SplitDateTime, jt.IsStarted, jt.DateStarted }).ToList();
             
            
             bool? isStarted = false;
             bool? isSplit = false;
             DateTime result = DateTime.Now;

             if (query.Count > 0)
             {
                 foreach (var item in query)
                 {
                     isStarted = item.IsStarted;// bool.Parse(row["IsStarted"].ToString());
                     if (isSplit == true)
                     {
                         if(item.SplitDateTime != null)
                         {
                             result = DateTime.Parse(item.SplitDateTime.ToString());
                         }
                     }
                     else
                     {
                         if (isStarted == true)
                         {
                             if (item.DateStarted != null)
                             {
                                 result = DateTime.Parse(item.DateStarted.ToString());
                             }
                         }
                     }
                 }
             }
             return result;
         }

         public  DateTime GetLatestScanTime(int travelerId)
         {
             string query1 = @"SELECT tblJobTracking.EntryTime 
                            FROM tblJobTracking, tblTravelerHeaders 
                            WHERE tblTravelerHeaders.TravelerId = @TravelerId  AND 
                            tblJobTracking.EntryId = tblTravelerHeaders.LatestScanId ";
             var query = (from jt in dbContext.tblJobTrackings
                          join th in dbContext.tblTravelerHeaders
                              on jt.TravelerID equals th.TravelerId
                          where th.TravelerId == travelerId && jt.EntryID == th.LatestScanId
                          select jt.EntryTime).ToList();
             DateTime latestScanTime = DateTime.Now;
             if (query.Count > 0)
             {
                 foreach (var item in query)
                 {
                     if (item != null)
                     {
                         latestScanTime = item.Value;
                     }
                 }
             }
             return latestScanTime;
         }

         public string GetActName(int travelerId)
         {
             string query1 = @"SELECT tblTravelerHeaders.Travelerid, 
                      tblTravelerHeaders.TravelerName,tblActivities.Activityid, 
                      tblActivities.ActivityName 
                      FROM (((tblItems INNER JOIN tblTravelerHeaders ON tblItems.ItemID = tblTravelerHeaders.ItemID) 
                      INNER JOIN (tblJobTracking INNER JOIN tblActivities ON tblJobTracking.ActivityID = tblActivities.ActivityId) 
                      ON (tblJobTracking.TravelerID = tblTravelerHeaders.TravelerId) 
                      AND (tblTravelerHeaders.LatestScanId = tblJobTracking.EntryID)) 
                      INNER JOIN tblJobStatus ON tblTravelerHeaders.JobID = tblJobStatus.JobId) 
                      INNER JOIN tblMaterialTypes ON tblItems.MaterialID = tblMaterialTypes.MaterialId 
                      WHERE tblTravelerHeaders.TravelerID = @travelerId";
             var query = (from th in dbContext.tblTravelerHeaders
                          join jt in dbContext.tblJobTrackings
                              on th.TravelerId equals jt.TravelerID
                          join activities in dbContext.tblActivities
                              on jt.ActivityID equals activities.ActivityId
                          where th.TravelerId == travelerId && th.LatestScanId == jt.EntryID
                          select new { activities.ActivityName }).ToList();
             //CommonDataManager manager = new CommonDataManager(query);
             //manager.AddWithValue("@travelerid", travelerId);
             string actName = "";
             //DataTable dt = manager.GetDataTable();

             if (query.Count > 0)
             {
                 foreach (var item in query)
                 {
                     if (query != null)
                     {
                         actName = item.ActivityName;// row["ActivityName"].ToString();
                     }
                 }
             }
             return actName;
         }

         public  string GetInitialQuantity(int travelerId)
         {
             string sql = @"SELECT tblTravelerHeaders.QtyToBeMade
                      FROM (((tblItems INNER JOIN tblTravelerHeaders ON tblItems.ItemID = tblTravelerHeaders.ItemID) 
                      INNER JOIN (tblJobTracking INNER JOIN tblActivities ON tblJobTracking.ActivityID = tblActivities.ActivityId) 
                      ON (tblJobTracking.TravelerID = tblTravelerHeaders.TravelerId) 
                      AND (tblTravelerHeaders.LatestScanId = tblJobTracking.EntryID))  
                      INNER JOIN tblJobStatus ON tblTravelerHeaders.JobID = tblJobStatus.JobId)  
                      INNER JOIN tblMaterialTypes ON tblItems.MaterialID = tblMaterialTypes.MaterialId 
                      WHERE tblTravelerHeaders.TravelerID = @travelerid";
             SqlParameter param = new SqlParameter("@travelerid", travelerId);
             var query = dbContext.Database.SqlQuery<double?>(sql, param).ToList();
             string cantidad = "";
             if (query.Count > 0)
             {
                 foreach (var items in query)
                 {
                     cantidad = items.ToString();/// row["QtyToBeMade"].ToString();
                 }
             }
             return cantidad;
         }

         public string GetCurrentQuantity(int travelerId)
         {
             string sql = @"SELECT tblJobTracking.qty 
                                FROM (((tblItems INNER JOIN tblTravelerHeaders ON tblItems.ItemID = tblTravelerHeaders.ItemID) 
                                INNER JOIN (tblJobTracking INNER JOIN tblActivities ON tblJobTracking.ActivityID = tblActivities.ActivityId) 
                                ON (tblJobTracking.TravelerID = tblTravelerHeaders.TravelerId) 
                                AND (tblTravelerHeaders.LatestScanId = tblJobTracking.EntryID)) 
                                INNER JOIN tblJobStatus ON tblTravelerHeaders.JobID = tblJobStatus.JobId) 
                                INNER JOIN tblMaterialTypes ON tblItems.MaterialID = tblMaterialTypes.MaterialId 
                                WHERE tblTravelerHeaders.TravelerID = @travelerid";
             SqlParameter param = new SqlParameter("@travelerid", travelerId);
             var query = dbContext.Database.SqlQuery<double?>(sql, param).ToList();
             string trackQTY = "";

             if (query.Count > 0)
             {
                 foreach (var item in query)
                 {
                     trackQTY = item.ToString();// row["Qty"].ToString();
                 }
             }
             return trackQTY;
         }

         public  string GetSituation(int travelerId)
         {
             string sql = @"SELECT tblJobTracking.Ubicacion
                          FROM (((tblItems INNER JOIN tblTravelerHeaders ON tblItems.ItemID = tblTravelerHeaders.ItemID) 
                          INNER JOIN (tblJobTracking INNER JOIN tblActivities ON tblJobTracking.ActivityID = tblActivities.ActivityId) 
                          ON (tblJobTracking.TravelerID = tblTravelerHeaders.TravelerId) 
                          AND (tblTravelerHeaders.LatestScanId = tblJobTracking.EntryID)) 
                          INNER JOIN tblJobStatus ON tblTravelerHeaders.JobID = tblJobStatus.JobId) 
                          INNER JOIN tblMaterialTypes ON tblItems.MaterialID = tblMaterialTypes.MaterialId 
                          WHERE tblTravelerHeaders.TravelerID = @travelerid";
             SqlParameter param = new SqlParameter("@travelerid", travelerId);
             var query = dbContext.Database.SqlQuery<string>(sql, param).ToList();
             string situacion = "";

             if (query.Count > 0)
             {
                 foreach (var item in query)
                 {
                     if (item != null)
                     {
                         situacion = item.ToString();// row["Ubicacion"].ToString() + " " + row["Proceso"].ToString();
                     } 
                 }
             }
             return situacion;
         }

         public  string GetAuxWeight(int travelerId)
         {
             string sql = @"SELECT tblJobTracking.AuxWeight 
                          FROM (((tblItems INNER JOIN tblTravelerHeaders ON tblItems.ItemID = tblTravelerHeaders.ItemID) 
                          INNER JOIN (tblJobTracking INNER JOIN tblActivities ON tblJobTracking.ActivityID = tblActivities.ActivityId) 
                          ON (tblJobTracking.TravelerID = tblTravelerHeaders.TravelerId) 
                          AND (tblTravelerHeaders.LatestScanId = tblJobTracking.EntryID)) 
                          INNER JOIN tblJobStatus ON tblTravelerHeaders.JobID = tblJobStatus.JobId) 
                          INNER JOIN tblMaterialTypes ON tblItems.MaterialID = tblMaterialTypes.MaterialId 
                          WHERE tblTravelerHeaders.TravelerID = @Travelerid";
             SqlParameter param = new SqlParameter("@Travelerid", travelerId);
             var query = dbContext.Database.SqlQuery<double?>(sql, param).ToList();
             string auxWeight = "0";

             if (query.Count > 0)
             {
                 foreach (var item in query)
                 {
                     if (item != null)
                     {
                         auxWeight = item.Value.ToString();// row["AuxWeight"].ToString();
                     }
                 }
             }
             return auxWeight;
         }
    }
}