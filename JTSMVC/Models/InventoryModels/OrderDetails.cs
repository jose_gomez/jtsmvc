﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JTSMVC.Models.InventoryModels
{
    public class OrderDetails
    {
        private List<string> alternatives;
        private string orderDate;
        private string dateCreated;
        private string createdBy;
        private string dateWanted;
        private int orderId;

        public int OrderId
        {
            get { return orderId; }
            set { orderId = value; }
        }

        public string DateWanted
        {
            get { return dateWanted; }
            set { dateWanted = value; }
        }

        public string OrderDate
        {
            get { return orderDate; }
            set { orderDate = value; }
        }

        public string DateCreated
        {
            get { return dateCreated; }
            set { dateCreated = value; }
        }

        public string CreatedBy
        {
            get { return createdBy; }
            set { createdBy = value; }
        }

        public List<string> Alternatives
        {
            get { return alternatives; }
            set { alternatives = value; }
        }
    }
}