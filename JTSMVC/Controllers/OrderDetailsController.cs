﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection.Emit;
using System.Web.Http;
using System.Web.Script.Serialization;

namespace JTSMVC.Models.InventoryModels
{
    public class OrderDetailsController : ApiController
    {
        public IEnumerable<dynamic> Get(string orderNumber)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            JTSMVC.Models.Entities1 dbContext = new Models.Entities1();
            Newtonsoft.Json.JsonSerializer serializerJson = new Newtonsoft.Json.JsonSerializer();
            var query = dbContext.tblOrders.Where(o => (o.FactoryOrderNumber == orderNumber || o.OrderNumber == orderNumber) && o.IsCancelled == false && o.IsComplete == false).Select(o => new
            {
                Ordernumber = o.FactoryOrderNumber,
                o.OrderDate,
                o.DateWanted,
                o.Creada,
                o.dtStamp,
                o.OrderID
            }).FirstOrDefault();

            var queryToUnite = (from th in dbContext.tblTravelerHeaders join vit in dbContext.tblVirtualInventories on th.ItemID equals vit.ItemId where th.TravelerName == orderNumber
                                && th.ItemID == vit.ItemId && th.OrderID == vit.OrderId && th.IsComplete == false && th.IsCancelled == false && vit.OrderId > 0 && vit.SrcType.ToLower() == "j" && 
                                vit.FulfillmentType.ToLower() == "j" orderby vit.DatePlaced  descending select new 
            { 
                th.TravelerName, 
                vit.DatePlaced,
                vit.DateExpected,
                vit.OrderNumber,
                dtStamp = vit.DatePlaced,
                OrderId =vit.SrcId1
            }).FirstOrDefault();

            string sql = @"SELECT i.ItemNumber
                            FROM tblItems AS i, tblOrderItems AS o WHERE o.ItemID = i.ItemID AND o.IsComplete = 0 AND
                            o.IsCancelled = 0 AND o.IsJobSetup = 1 and o.OrderId =@orderId
                            UNION 
                            SELECT (Select ItemNumber from tblItems where ItemId = vi.ItemId)
                            FROM tblVirtualInventory AS vi 
                            WHERE vi.SrcType = 'J' AND vi.OrderID > 0 AND vi.SrcId1 = @orderId and
                            vi.SrcId2 > 0 AND vi.FulfillmentType = 'J';";
            SqlParameter param = new SqlParameter();

               List<OrderDetails> orderDetails = new List<OrderDetails>();
               if (query != null)
               {
                   var order = new OrderDetails()
                   {
                       OrderDate = DateTime.Parse(query.OrderDate.ToString()).ToShortDateString(),
                       DateWanted = DateTime.Parse(query.DateWanted.ToString()).ToShortDateString(),
                       CreatedBy = query.Creada,
                       DateCreated = (query.dtStamp != null)?DateTime.Parse(query.dtStamp.ToString()).ToShortDateString():string.Empty,
                       OrderId= query.OrderID
                   };
                   param.Value = query.OrderID;
                   param.ParameterName = "@orderId";
                   var styles = dbContext.Database.SqlQuery<string>(sql, param);
                   order.Alternatives = styles.ToList<string>();
                   orderDetails.Add(order);
               }
               else if (queryToUnite != null)
               {
                   var order = new OrderDetails()
                   {
                       OrderDate = (queryToUnite.DatePlaced != null)? DateTime.Parse(queryToUnite.DatePlaced.ToString()).ToShortDateString():"N/A",
                       DateWanted = (queryToUnite.DateExpected != null)?DateTime.Parse(queryToUnite.DateExpected.ToString()).ToShortDateString():"N/A",
                       CreatedBy = queryToUnite.OrderNumber,
                       DateCreated = (queryToUnite.dtStamp!=null)?DateTime.Parse(queryToUnite.dtStamp.ToString()).ToShortDateString():"N/A",
                       OrderId = (queryToUnite.OrderId != null)?(int)queryToUnite.OrderId:0
                   };
                   param.Value = queryToUnite.OrderId;
                   param.ParameterName = "@orderId";
                   var styles = dbContext.Database.SqlQuery<string>(sql, param);
                   order.Alternatives = styles.ToList<string>();
                   orderDetails.Add(order);
               }
               return (orderDetails.Count() > 0) ? orderDetails.ToArray() : null;
        }
    }
}
