﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using JTSMVC.Models;

namespace JTSMVC.Controllers
{
    public class OnHandDetailsController : Controller
    {
        Entities1 dbContext = new Entities1();
        public ActionResult Index(string itemNumber, string FactoryOrderNumber)
        {
            FactoryOrderNumber = Server.HtmlDecode(FactoryOrderNumber);
            ViewBag.itemNumber = itemNumber;
            int itemId = dbContext.tblItems.Where(it => it.ItemNumber == itemNumber).Select(it=>it.ItemID).FirstOrDefault();
            bool isOrder = (dbContext.tblOrders.Where(o => o.FactoryOrderNumber.ToLower() == FactoryOrderNumber.ToLower()).FirstOrDefault() != null);
            SqlParameter param = new SqlParameter("OrderNumber",FactoryOrderNumber);
             int orderId = dbContext.Database.SqlQuery<int>(
                @"SELECT o.OrderID as ID
                            FROM tblOrders AS o WHERE o.IsComplete = 0 AND o.IsCancelled = 0 and (o.ordernumber = @OrderNumber or o.FactoryOrderNumber = @OrderNumber)
                            UNION SELECT 
                            vi.SrcId1 AS ID
                            FROM tblVirtualInventory AS vi, tblTravelerHeaders AS th WHERE th.ItemID = vi.ItemID AND 
                            th.OrderID = vi.OrderID AND th.IsComplete = 0 AND th.IsCancelled = 0 AND vi.OrderID > 0 AND  th.travelername = @orderNumber and
                            vi.SrcType = 'J' AND vi.FulfillmentType = 'J'", param

                ).FirstOrDefault();

             var query = (from lot in dbContext.tblLots
                          join con in dbContext.tblContainers on lot.LotId equals con.LotId into locLoc
                          from x in locLoc.DefaultIfEmpty()
                          join locations in dbContext.tblLocations on x.LocationId equals locations.LocationId into locCon
                          from y in locCon.DefaultIfEmpty()
                          where lot.IsActive == true && x.IsEmpty == false && x.Tipo.ToLower() != "reparaciones" && x.IsDeleted == false && lot.ItemId == itemId
                          && x.OrderID == ((isOrder) ? orderId : x.OrderID) && x.ContainerCode == FactoryOrderNumber
                          select new
                          {
                              location = y.LocationDescriptionEn,
                              defaultPieceItemWeight = lot.DefaultPieceItemWeight,
                              AuxW = x.AuxWeight,
                              ContainerPieceWeight = x.DefaultPieceItemWeight,
                              order = x.OrderNumber,
                              date = lot.DateCreated,
                              code = x.ContainerCode
                          }
                          ).FirstOrDefault();
             OnHandDetails details = new OnHandDetails();
             details.AuxWeight = (query.AuxW != null) ? query.AuxW : 0;
             details.Location = query.location;
             details.OrderNo = query.order;
             details.OrderDate = (query.date != null)?query.date:null;
             details.Container = query.code;
             details.ContainerPieceWeight = (query.ContainerPieceWeight != null)?query.ContainerPieceWeight:0;
             
             ViewBag.itemNumber = itemNumber;
             ViewBag.details = details;
            return View();
        }

        public ActionResult Debits(string itemNumber, string FactoryOrderNumber)
        {
            int itemId = dbContext.tblItems.Where(it => it.ItemNumber == itemNumber).Select(it => it.ItemID).FirstOrDefault();
            bool isOrder = (dbContext.tblOrders.Where(o => o.FactoryOrderNumber.ToLower() == FactoryOrderNumber.ToLower()).FirstOrDefault() != null);
            SqlParameter param = new SqlParameter("OrderNumber", FactoryOrderNumber);
            int orderId = dbContext.Database.SqlQuery<int>(
               @"SELECT o.OrderID as ID
                            FROM tblOrders AS o WHERE o.IsComplete = 0 AND o.IsCancelled = 0 and (o.ordernumber = @OrderNumber or o.FactoryOrderNumber = @OrderNumber)
                            UNION SELECT 
                            vi.SrcId1 AS ID
                            FROM tblVirtualInventory AS vi, tblTravelerHeaders AS th WHERE th.ItemID = vi.ItemID AND 
                            th.OrderID = vi.OrderID AND th.IsComplete = 0 AND th.IsCancelled = 0 AND vi.OrderID > 0 AND  th.travelername = @orderNumber and
                            vi.SrcType = 'J' AND vi.FulfillmentType = 'J'", param

               ).FirstOrDefault();

            var resultSet = (from vi in dbContext.tblVirtualInventories
                         join orderItems in dbContext.tblOrderItems
                             on vi.SrcId2 equals orderItems.OrderLineItemId
                         join o in dbContext.tblOrders
                             on orderItems.OrderID equals o.OrderID
                         join cus in dbContext.tblCustomers
                             on o.CustomerID equals cus.CustomerID
                         join th in dbContext.tblTravelerHeaders on orderItems.OrderID equals th.OrderID
                         where vi.SrcType.ToLower() == "o" && vi.ItemId == itemId && vi.Qty < 0 && o.OrderID == ((isOrder)?orderId:o.OrderID) && th.OrderLine == orderItems.LineNumber
                         select new
                         {
                             DateNeeded = orderItems.DateWanted,
                             Customer = orderItems.Instructions,
                             StyleOrOrder = o.FactoryOrderNumber,
                             CreationDate = vi.DatePlaced,
                             Type = "o",
                             Comments = orderItems.LineIntructions,
                             orderitemid = orderItems.OrderLineItemId,
                             LineNumber = orderItems.LineNumber
                         }
                             ).Concat(
                                from vi in dbContext.tblVirtualInventories
                                join th in dbContext.tblTravelerHeaders
                                    on vi.SrcId1 equals th.TravelerId
                                join orderItems in dbContext.tblOrderItems
                                 on th.OrderID equals orderItems.OrderID
                                join items in dbContext.tblItems
                                on th.ItemID equals items.ItemID
                                where vi.SrcType.ToLower() == "j" && vi.ItemId == th.ItemID
                                && th.TravelerName.ToLower() == FactoryOrderNumber.ToLower() && th.OrderLine == orderItems.LineNumber
                                select new
                                {
                                    DateNeeded = vi.DateNeeded,
                                    Customer = orderItems.Instructions,
                                    StyleOrOrder = items.ItemNumber,
                                    CreationDate = vi.DatePlaced,
                                    Type = "j",
                                    Comments = th.Comentario,
                                    orderitemid = orderItems.OrderLineItemId,
                                    LineNumber = orderItems.LineNumber
                                }
                             ).Distinct().ToList();
            var query = resultSet.FirstOrDefault();
            if (resultSet.Count > 1 && !isOrder)
            {
                query = resultSet.Where(rs => (rs.StyleOrOrder + "." + rs.LineNumber) == FactoryOrderNumber).Select(rs => rs).FirstOrDefault();
            }
            if (query != null)
            {
                var type = query.Type;

                DebitDetails details = new DebitDetails();
                if (query.DateNeeded != null)
                {
                    details.DateNeeded = ((DateTime)query.DateNeeded).ToShortDateString();
                }
                if (query.CreationDate != null)
                {
                    details.DateCreated = ((DateTime)query.CreationDate).ToShortDateString();
                }
                details.Style = query.StyleOrOrder;
                details.Customer = query.Customer;
                details.Comments = query.Comments;

                ViewBag.Type = type;
                ViewBag.details = details;
            }
            return View();
        }

        public ActionResult Credits(string itemNumber, string FactoryOrderNumber)
        {
            if (string.IsNullOrWhiteSpace(itemNumber) || string.IsNullOrWhiteSpace(FactoryOrderNumber))
            {
                return null;
            }
            int itemId = dbContext.tblItems.Where(it => it.ItemNumber == itemNumber).Select(it => it.ItemID).FirstOrDefault();
            bool isOrder = (dbContext.tblOrders.Where(o => o.FactoryOrderNumber.ToLower() == FactoryOrderNumber.ToLower()).FirstOrDefault() != null);
            SqlParameter param = new SqlParameter("OrderNumber", FactoryOrderNumber);
            int orderId = dbContext.Database.SqlQuery<int>(
               @"SELECT o.OrderID as ID
                            FROM tblOrders AS o WHERE o.IsComplete = 0 AND o.IsCancelled = 0 and (o.ordernumber = @OrderNumber or o.FactoryOrderNumber = @OrderNumber)
                            UNION SELECT 
                            vi.SrcId1 AS ID
                            FROM tblVirtualInventory AS vi, tblTravelerHeaders AS th WHERE th.ItemID = vi.ItemID AND 
                            th.OrderID = vi.OrderID AND th.IsComplete = 0 AND th.IsCancelled = 0 AND vi.OrderID > 0 AND  th.travelername = @orderNumber and
                            vi.SrcType = 'J' AND vi.FulfillmentType = 'J'", param

               ).FirstOrDefault();

            var travelers = (from th in dbContext.tblTravelerHeaders
                             join vi in dbContext.tblVirtualInventories
                                 on th.TravelerId equals vi.SrcId1
                             where vi.SrcType.ToLower() == "j" && vi.Qty > 0 && vi.ItemId == itemId & vi.OrderId == ((orderId > 0) ? orderId : vi.OrderId)
                             select new { th.TravelerId, th.TravelerName }).ToList();

            int travelerId = travelers.Where(t => t.TravelerName.ToLower() == FactoryOrderNumber.ToLower()).FirstOrDefault().TravelerId;
          
            InventoryCreditsUtility utility = new InventoryCreditsUtility();

      
            string nextActivity = utility.GetNextActivityInOut((int)travelerId);
            DateTime dateCreated = utility.GetDateCreated((int)travelerId);
            DateTime lastestScanTime = utility.GetLatestScanTime((int)travelerId);
            string activityName = utility.GetActName((int)travelerId);
            string qtyToBeMade = utility.GetInitialQuantity((int)travelerId);
            string currentQuantity = utility.GetCurrentQuantity((int)travelerId);
            string situation = utility.GetSituation((int)travelerId);
            var traveler = (from th in dbContext.tblTravelerHeaders where th.TravelerId == travelerId select new { th.Comentario, th.Number }).FirstOrDefault();
            string comments = traveler.Comentario;
            string orderNumber = traveler.Number;
            string auxWeight = utility.GetAuxWeight((int)travelerId);
            decimal divider = 0;
            if (decimal.Parse(auxWeight) > 0 && decimal.Parse(currentQuantity) > 0)
            {
               divider =  decimal.Parse(auxWeight) / decimal.Parse(currentQuantity);
            }
            if (!string.IsNullOrEmpty(qtyToBeMade))
            {
                if (float.Parse(currentQuantity) > float.Parse(qtyToBeMade))
                {
                    auxWeight = Math.Round(divider, 2).ToString();
                }
            }
            string ordersWeightPart = Math.Round(double.Parse(auxWeight), 2).ToString();
            CreditDetails credits = new CreditDetails();
            credits.NextActivity = nextActivity;
            credits.DateCreated = dateCreated;
            credits.LastestScanTime = lastestScanTime;
            credits.ActivityName = activityName;
            credits.QtyToBeMade = qtyToBeMade;
            credits.CurrentQuantity = currentQuantity;
            credits.Situation = situation;
            credits.Comments = comments;
            credits.OrderNumber = orderNumber;
            credits.AuxWeight = auxWeight;
            credits.OrdersWeightPart = ordersWeightPart;
            return View(credits);
        }

        

    }
}
