﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JTSMVC.Models
{
    public class DebitDetails
    {
        private string dateNeeded;
        private string customer;
        private string style;
        private string dateCreated;
        private string comments;

        public string DateNeeded
        {
            get { return dateNeeded; }
            set { dateNeeded = value; }
        }

        public string Customer
        {
            get { return customer; }
            set { customer = value; }
        }

        public string Style
        {
            get { return style; }
            set { style = value; }
        }

        public string DateCreated
        {
            get { return dateCreated; }
            set { dateCreated = value; }
        }

        public string Comments
        {
            get { return comments; }
            set { comments = value; }
        }
    }
}