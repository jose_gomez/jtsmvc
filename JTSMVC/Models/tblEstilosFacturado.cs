//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace JTSMVC.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblEstilosFacturado
    {
        public int LineaId { get; set; }
        public double FactId { get; set; }
        public Nullable<System.DateTime> Fecha_factura { get; set; }
        public Nullable<System.DateTime> Hora_grabado { get; set; }
        public Nullable<int> Numero_factura { get; set; }
        public string Orden { get; set; }
        public string Estilo { get; set; }
        public Nullable<short> Bag { get; set; }
        public string Descripcion { get; set; }
        public string Kilate { get; set; }
        public Nullable<double> weigth_Neto { get; set; }
        public Nullable<double> Weight_bruto { get; set; }
        public Nullable<decimal> Precio_Oro { get; set; }
        public Nullable<decimal> Precio_gramo { get; set; }
        public Nullable<decimal> Gold_Cost { get; set; }
        public Nullable<decimal> Precio_labor { get; set; }
        public Nullable<decimal> Labor_cost { get; set; }
        public Nullable<decimal> Shipping_Cost { get; set; }
        public string Numero_cuenta { get; set; }
        public Nullable<double> Cantidad_piezas { get; set; }
        public Nullable<decimal> Amount { get; set; }
        public Nullable<short> Numero_suplidor { get; set; }
        public string Status { get; set; }
        public string Empacado { get; set; }
        public Nullable<double> Pack { get; set; }
        public Nullable<int> Caja { get; set; }
        public Nullable<bool> Editar { get; set; }
        public Nullable<double> StagingPrice { get; set; }
        public Nullable<double> TotalStaging { get; set; }
        public string Linea { get; set; }
        public string Serie { get; set; }
        public Nullable<double> QtyShipped { get; set; }
        public Nullable<double> OrderId { get; set; }
        public Nullable<double> ItemId { get; set; }
        public Nullable<bool> Enviado { get; set; }
        public Nullable<double> FineGold { get; set; }
        public Nullable<bool> Manual { get; set; }
        public Nullable<double> GoldNet { get; set; }
        public Nullable<double> SilvNet { get; set; }
        public Nullable<decimal> Precio_Plata { get; set; }
        public Nullable<decimal> Precio_GPlata { get; set; }
        public Nullable<double> Qty { get; set; }
        public Nullable<double> PesoItem { get; set; }
        public string BuyList { get; set; }
        public string Contenedor { get; set; }
        public Nullable<double> ContainerID { get; set; }
        public Nullable<bool> Cerrada { get; set; }
        public string PaisOrigen { get; set; }
        public Nullable<bool> SubContratado { get; set; }
        public Nullable<System.DateTime> Fecha_Embarcado { get; set; }
        public Nullable<double> Peso_NoMetal { get; set; }
        public Nullable<double> Nikel { get; set; }
        public Nullable<double> GoldMarket { get; set; }
        public Nullable<double> SilverMarket { get; set; }
        public Nullable<double> OtroMetal { get; set; }
        public Nullable<double> PesoGF { get; set; }
        public Nullable<double> PrecioNikel { get; set; }
        public string Codigo_Alterno { get; set; }
        public Nullable<double> FineSilver { get; set; }
        public Nullable<double> Precio_Piedra { get; set; }
    }
}
