//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace JTSMVC.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblPurchaseOrder
    {
        public int POid { get; set; }
        public string POnumber { get; set; }
        public Nullable<int> VendorId { get; set; }
        public string Priority { get; set; }
        public Nullable<System.DateTime> DateWanted { get; set; }
        public Nullable<System.DateTime> DatePlaced { get; set; }
        public string Notes { get; set; }
        public Nullable<System.DateTime> DateExpected { get; set; }
        public bool IsSent { get; set; }
        public Nullable<System.DateTime> DateSent { get; set; }
        public bool IsReceived { get; set; }
        public Nullable<System.DateTime> DateReceived { get; set; }
        public string POtransfer { get; set; }
        public string Invoice { get; set; }
        public Nullable<double> LoveEarth { get; set; }
    }
}
