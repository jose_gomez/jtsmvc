﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JTSMVC.Models
{
    public class CreditDetails
    {
        public string NextActivity { get; set; }
        public DateTime? DateCreated { get; set; }
        public DateTime? LastestScanTime { get; set; }
        public string ActivityName { get; set; }
        public string QtyToBeMade { get; set; }
        public string CurrentQuantity { get; set; }
        public string Situation { get; set; }
        public string Comments { get; set; }
        public string OrderNumber { get; set; }
        public string AuxWeight { get; set; }
        public string OrdersWeightPart { get; set; }
    }
}