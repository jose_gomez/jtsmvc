﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;

namespace JTSMVC.Models.InventoryModels
{
    public class OrderDetailsController : ApiController
    {
        public IEnumerable<dynamic> Get(string orderNumber)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            JTSMVC.Models.JTSEntitiesDBContext dbContext = new Models.JTSEntitiesDBContext();
            Newtonsoft.Json.JsonSerializer serializerJson = new Newtonsoft.Json.JsonSerializer();
            var query = dbContext.tblOrders.Where(o => o.FactoryOrderNumber == orderNumber && o.IsCancelled == false && o.IsComplete == false).Select(o => new
            {
                Ordernumber = o.FactoryOrderNumber,
                o.OrderDate,
                o.DateWanted,
                o.Creada,
                o.dtStamp
            }).FirstOrDefault();

            var queryToUnite = (from th in dbContext.tblTravelerHeaders join vit in dbContext.tblVirtualInventories on th.ItemID equals vit.ItemId where th.TravelerName == orderNumber
                                && th.ItemID == vit.ItemId && th.OrderID == vit.OrderId && th.IsComplete == false && th.IsCancelled == false && vit.OrderId > 0 && vit.SrcType.ToLower() == "j" && 
                                vit.FulfillmentType.ToLower() == "j" orderby vit.DatePlaced  descending select new 
            { 
                th.TravelerName, 
                vit.DatePlaced,
                vit.DateExpected,
                vit.OrderNumber,
                dtStamp = vit.DatePlaced 
            }).FirstOrDefault();

           

               List<OrderDetails> orderDetails = new List<OrderDetails>();
               if (query != null)
               {
                   var order = new OrderDetails()
                   {
                       OrderDate = DateTime.Parse(query.OrderDate.ToString()).ToShortDateString(),
                       DateWanted = DateTime.Parse(query.DateWanted.ToString()).ToShortDateString(),
                       CreatedBy = query.Creada,
                       DateCreated = DateTime.Parse(query.dtStamp.ToString()).ToShortDateString()
                   };
                   orderDetails.Add(order);
               }
               else if (queryToUnite != null)
               {
                   var order = new OrderDetails()
                   {
                       OrderDate = (queryToUnite.DatePlaced != null)? DateTime.Parse(queryToUnite.DatePlaced.ToString()).ToShortDateString():string.Empty,
                       DateWanted = (queryToUnite.DateExpected != null)?DateTime.Parse(queryToUnite.DateExpected.ToString()).ToShortDateString():string.Empty,
                       CreatedBy = queryToUnite.OrderNumber,
                       DateCreated = (queryToUnite.dtStamp!=null)?DateTime.Parse(queryToUnite.dtStamp.ToString()).ToShortDateString():string.Empty
                   };
                   orderDetails.Add(order);
               }
               return (orderDetails.Count() > 0) ? orderDetails.ToArray() : null;
        }
    }
}
