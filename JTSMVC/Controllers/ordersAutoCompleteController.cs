﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection.Emit;
using System.Web.Http;
using JTSMVC.Models;

namespace JTSMVC.Controllers
{
    public class ordersAutoCompleteController : ApiController
    {
        public IEnumerable<string> Get(string order)
        {
            JTSMVC.Models.Entities1 entities = new Entities1();


            SqlParameter param = new SqlParameter("OrderNumber", "%" + order + "%");
            var orders = entities.Database.SqlQuery<string>(@"SELECT top 10 o.OrderNumber as Orders
                            FROM tblOrders as o inner join tblOrderITems as oi on o.OrderId = oi.OrderId  WHERE o.IsComplete = 0 AND o.IsCancelled = 0 and oi.IsComplete = 0 and oi.isCancelled = 0
                            and OrderNumber like @OrderNumber
                            UNION  all
                            SELECT top 10 
                            th.TravelerName as OrderNumber
                            FROM tblVirtualInventory AS vi, tblTravelerHeaders AS th
                            WHERE th.ItemID = vi.ItemID AND th.TravelerName like @OrderNumber and
                            th.OrderID = vi.OrderID AND th.IsComplete = 0 AND th.IsCancelled = 0 AND vi.OrderID > 0 AND vi.SrcType = 'J' AND 
                            vi.FulfillmentType = 'J'", param).Distinct().Take(10).ToList();
            return orders;
        }
    }
}
