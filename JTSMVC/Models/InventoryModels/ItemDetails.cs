﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JTSMVC.Models.InventoryModels
{
    public class ItemDetails
    {
        private double defaultWeight;
        private string category;
        private int itemId;
        private string itemNumber;

        public double Min
        {
            get {
                if (defaultWeight > 0)
                {
                    return defaultWeight - 0.05;
                }
                else
                {
                    throw new InvalidOperationException("The defaultWeight property needs to have a value for this property to return something");
                }
            }
        }

        public double Max
        {
            get {
                if (defaultWeight > 0)
                {
                    return defaultWeight + 0.05;
                }
                else
                {
                    throw new InvalidOperationException("The defaultWeight property needs to have a value for this property to return something");
                }
            }
        }

        public double DefaultWeight
        {
            get { return defaultWeight; }
            set { defaultWeight = value; }
        }

        public string Category
        {
            get { return category; }
            set { category = value; }
        }

        public int ItemId
        {
            get { return itemId; }
            set { itemId = value; }
        }

        public string ItemNumber
        {
            get { return itemNumber; }
            set { itemNumber = value; }
        }


    }
}