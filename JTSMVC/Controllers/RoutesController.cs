﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using JTSMVC.Models;

namespace JTSMVC.Controllers
{
    public class RoutesController : Controller
    {
        //
        // GET: /Routes/

        public ActionResult Index(string itemNumber)
        {
            Entities1 dbContext = new Entities1();
            string sql = @"SELECT  tblActivities.ActivityName as ActivityName, 
                            tblRutas.Instructions 
                            FROM ((tblRutas INNER JOIN tblActivities 
                            ON tblRutas.ActivityId = tblActivities.ActivityId) 
                            INNER JOIN tblItems ON tblRutas.ItemId = tblItems.ItemID) 
                            WHERE (tblRutas.ItemId = @ItemId)";
            int itemId = dbContext.tblItems.Where(it => it.ItemNumber == itemNumber).Select(it => it.ItemID).FirstOrDefault();
            SqlParameter param = new SqlParameter("@ItemId", itemId);
            List<Routes> list = new List<Routes>();
            var query = dbContext.Database.SqlQuery<Routes>(sql, param);
            if (query != null)
            {
                foreach (Routes item in query)
                {
                    list.Add(item);
                }
            }
            return View(list);
        }
       
    }

    public class Routes
    {
        public string ActivityName { get; set; }
        public string Instructions { get; set; }
    }
}
