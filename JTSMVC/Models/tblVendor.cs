//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace JTSMVC.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblVendor
    {
        public int VendorID { get; set; }
        public string VendorNumber { get; set; }
        public string VendorName { get; set; }
        public string StreetAddress { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string ContactName { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public Nullable<int> LeadTime { get; set; }
        public string Pais { get; set; }
    }
}
