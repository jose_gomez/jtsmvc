//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace JTSMVC.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblSubActivity
    {
        public int SubActID { get; set; }
        public Nullable<double> ActivityID { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public Nullable<double> WorkCellID { get; set; }
        public Nullable<double> StandardYield { get; set; }
        public Nullable<double> AverageYield { get; set; }
        public Nullable<int> NewMeasure { get; set; }
        public Nullable<double> YieldLow { get; set; }
        public Nullable<double> YieldHigh { get; set; }
        public Nullable<bool> CheckPieceWeight { get; set; }
        public Nullable<double> ActNo { get; set; }
    }
}
