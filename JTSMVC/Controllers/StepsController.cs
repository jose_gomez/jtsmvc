﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;
using JTSMVC.Models;

namespace JTSMVC.Controllers
{
    
    public class StepsController : Controller
    {
        public ActionResult index(string itemNumber)
        {
            Entities1 dbContext = new Entities1();
            string sql  = @"SELECT tblActivities.ActivityName as Step,
                            tblItems_1.ItemNumber AS Item
                            FROM ((((tblItems INNER JOIN 
                            tblItemBOMBRsteps ON tblItems.ItemID = tblItemBOMBRsteps.ItemId) 
                            LEFT OUTER JOIN tblItemBOMBRstepItems 
                            ON tblItemBOMBRsteps.StepID = tblItemBOMBRstepItems.StepID) 
                            LEFT OUTER JOIN tblItems tblItems_1 ON tblItemBOMBRstepItems.ItemID = tblItems_1.ItemID) 
                            INNER JOIN tblActivities ON tblItemBOMBRsteps.ActivityId = tblActivities.ActivityId) 
                            WHERE (tblItems.ItemNumber = @ItemNumber) 
                            ORDER BY tblItemBOMBRsteps.StepNo";
            SqlParameter param = new SqlParameter("@ItemNumber", itemNumber);
            var stepsResultSet = dbContext.Database.SqlQuery<Steps>(sql, param).ToList();
           
            return View(stepsResultSet);
        }

    }

    public class Steps
    {
        public string Item { get; set; }
        public string Step { get; set; }
        public Steps()
        {

        }
        public Steps(string item, string step){
            this.Item = item;
            this.Step = step;
        }
    }
}
