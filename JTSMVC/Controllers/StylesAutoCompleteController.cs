﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using JTSMVC.Models;

namespace JTSMVC.Controllers
{
    public class StylesAutoCompleteController : ApiController
    {
        [HttpGet]
        [HttpPost]
        public string[] Get(string itemNumber)
        {
            Entities1 dbContext = new Entities1();
            List<string> lista = new List<string>();
            var query = (from items in dbContext.tblItems where items.ItemNumber.Contains(itemNumber) select items.ItemNumber).Take(20).ToList();
            return query.ToArray();
        }
    }
}
