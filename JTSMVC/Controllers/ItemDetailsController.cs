﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;
using JTSMVC.Models;

namespace JTSMVC.Controllers
{
    public class ItemDetailsController : ApiController
    {
        public ItemDetails Get(string item)
        {
            Entities1 dbContext = new Entities1();
            var items = (from it in dbContext.tblItems
                         join cat in dbContext.tblItemCategories
                             on it.CatId equals cat.CatId where it.ItemNumber == item
                         select new 
                         { 
                            it.Description,
                            it.DefaultPieceItemWeight,
                            cat.CatName
                         }).FirstOrDefault();
            ItemDetails itemDetails = new ItemDetails();
            
            if (items.DefaultPieceItemWeight != null)
            {
                itemDetails.Weight = Math.Round((double)items.DefaultPieceItemWeight,2).ToString();
                double average = (double)items.DefaultPieceItemWeight * 0.05;
                itemDetails.Max = Math.Round((double)items.DefaultPieceItemWeight + average,2).ToString();
                itemDetails.Min = Math.Round((double)items.DefaultPieceItemWeight - average,2).ToString();
            }
            itemDetails.Series = items.CatName;
            itemDetails.Description = items.Description;
            return itemDetails;
        }
    }

    public class ItemDetails
    {
        public ItemDetails() { }

        public string Min { get; set; }
        public string Max { get; set; }
        public string Weight { get; set; }
        public string Description { get; set; }
        public string Series { get; set; }
    }
}
