﻿using System;
using System.Linq;
using JTSMVC;
using NUnit.Framework;

namespace JTSMVC.Tests
{
    [TestFixture]
    public class ItemsTests
    {
        
        [Test]
        public void Is_items_Min_Weight_Valid()
        {
            var average = 0.05;
            JTSMVC.Models.InventoryModels.ItemDetails details = new JTSMVC.Models.InventoryModels.ItemDetails();
            details.DefaultWeight = 0.79;
            var expected = details.DefaultWeight - average;
            Assert.AreEqual(details.Min, expected);
        }


        [Test]
        public void Is_items_Max_Weight_Valid()
        {
            var average = 0.05;
            JTSMVC.Models.InventoryModels.ItemDetails details = new JTSMVC.Models.InventoryModels.ItemDetails();
            details.DefaultWeight = 0.79;
            var expected = details.DefaultWeight + average;
            Assert.AreEqual(details.Max, expected);
        }
    }
}
