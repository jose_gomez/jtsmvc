﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;

namespace JTSMVC.Models
{
    public class Converter
    {
        public bool ConvertImage(object pic, string path)
        {
            byte[] imgData = (byte[])pic;
            if (imgData.Length > 0)
            {
                try
                {
                    MemoryStream memStream = new MemoryStream(imgData, true);
                    OleStripper stripper = new OleStripper(memStream);
                    Stream strippedStream = stripper.GetStrippedStream();
                    Bitmap bp = new Bitmap(strippedStream);
                    bp.Save(path);
                    return true;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }

            return false;
        }



    }
}