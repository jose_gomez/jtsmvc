﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace JTSMVC.Models
{
    public class PictureHelper
    {
        public static string GetPictureURL(string ItemNumber)
        {
            Entities1 dbContext = new Entities1();
            int? itemId = dbContext.tblItems.Where(item => item.ItemNumber == ItemNumber).Select(item => item.ItemID).FirstOrDefault();
            if (itemId == null)
                return "";
            string imageURL = "";
            string path = HttpContext.Current.Server.MapPath("../Images/styles/" + ItemNumber + ".jpg");
            if (itemId > 0)
            {
                if (!File.Exists(path))
                {
                    object pic = dbContext.tblImagenes.Where(img => img.ItemID == itemId).Select(img => img.Foto).FirstOrDefault();
                    if (pic == null) return "";
                    Converter converter = new Converter();
                    if (!converter.ConvertImage(pic, path))
                    {
                        OLE2IMG ole = new OLE2IMG();
                        MemoryStream mem = ole.Ole2Img(pic);
                        if (mem != null)
                        {
                            FileStream file = File.OpenWrite(path);
                            mem.WriteTo(file);
                            file.Flush();
                            file.Close();
                            mem.Close();
                            imageURL = "Images/styles/" + ItemNumber + ".jpg";
                        }
                    }
                    else
                    {
                        imageURL = "Images/styles/" + ItemNumber + ".jpg";
                    }

                }
                else
                {
                    imageURL = "Images/styles/" + ItemNumber + ".jpg";
                }
            }
            return imageURL;
        }
    }
}