﻿using System;
using System.Collections.Generic;
using System.Data.Objects.SqlClient;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using JTSMVC.Models;

namespace JTSMVC.Controllers
{
    public class OrdersTablesController : Controller
    {
        //
        // GET: /OrdersTables/

        public ActionResult Index()
        {
            return View();
        }

        public PartialViewResult GetTables(string ItemNumber, string factoryOrderNumber)
        {
            List<Tuple<string, double?>> onHand = new List<Tuple<string, double?>>();
            List<Tuple<string, double?>> debits = new List<Tuple<string, double?>>();
            List<Tuple<string, double?>> credits = new List<Tuple<string, double?>>();
            Entities1 dbContext = new Entities1();
            int itemId = dbContext.tblItems.Where(it => it.ItemNumber == ItemNumber).Select(it=>it.ItemID).FirstOrDefault();
            bool isOrder = (dbContext.tblOrders.Where(o => o.FactoryOrderNumber.ToLower() == factoryOrderNumber.ToLower() || o.OrderNumber.ToLower() == factoryOrderNumber.ToLower()).FirstOrDefault() != null);
            SqlParameter param = new SqlParameter("OrderNumber",factoryOrderNumber);
            int orderId = dbContext.Database.SqlQuery<int>(
                @"SELECT o.OrderID as ID
                            FROM tblOrders AS o WHERE o.IsComplete = 0 AND o.IsCancelled = 0 and o.ordernumber = @OrderNumber
                            UNION SELECT 
                            vi.SrcId1 AS ID
                            FROM tblVirtualInventory AS vi, tblTravelerHeaders AS th WHERE th.ItemID = vi.ItemID AND 
                            th.OrderID = vi.OrderID AND th.IsComplete = 0 AND th.IsCancelled = 0 AND vi.OrderID > 0 AND  th.travelername = @orderNumber and
                            vi.SrcType = 'J' AND vi.FulfillmentType = 'J'", param

                ).FirstOrDefault();
            var OnHandContainers = (from tc in dbContext.tblContainers join lt in dbContext.tblLots on tc.LotId equals lt.LotId where lt.ItemId == itemId &&
                                    lt.IsActive == true && tc.IsEmpty == false && tc.Tipo.ToLower() != "reparaciones" && tc.IsDeleted == false && tc.OrderID == ((isOrder)?orderId:tc.OrderID)
                                    select new { tc.ContainerCode, tc.Qty }).ToList();
            foreach (var item in OnHandContainers)
            {
                onHand.Add(new Tuple<string, double?>(item.ContainerCode, item.Qty));
            }

            ViewBag.OnHand = onHand;
          
            var debitsQuery = (from vi in dbContext.tblVirtualInventories
                               join orderItems in dbContext.tblOrderItems on  vi.SrcId2 equals orderItems.OrderLineItemId 
                               
                               join o in dbContext.tblOrders
                                   on orderItems.OrderID equals o.OrderID
                               where vi.SrcType.ToLower() == "o" && vi.ItemId == itemId && vi.Qty < 0 && o.OrderID == ((orderId>0) ? orderId : o.OrderID)
                               && orderItems.OrderLineItemId == vi.SrcId2
                               select new { Quantity = vi.Qty, Container = o.FactoryOrderNumber + "." + orderItems.LineNumber }).Concat(
                                from vi1 in dbContext.tblVirtualInventories
                                join th in dbContext.tblTravelerHeaders on vi1.SrcId1 equals th.TravelerId
                                where vi1.SrcType.ToLower() == "j" && vi1.Qty < 0 && vi1.SrcId1 == ((orderId>0) ? orderId : vi1.SrcId1) && vi1.ItemId == itemId
                                select new {Quantity = vi1.Qty, Container = th.TravelerName }
                               ).ToList();
            string sql = debitsQuery.ToString();
            foreach (var item in debitsQuery)
            {
                 debits.Add(new Tuple<string, double?>(item.Container, item.Quantity * -1));
            }

            ViewBag.debits = debits;

            var creditsQuery = (from vi in dbContext.tblVirtualInventories join th in dbContext.tblTravelerHeaders on vi.SrcId1 equals th.TravelerId 
                                where vi.SrcType.ToLower() == "j" && vi.ItemId == itemId && vi.Qty > 0 && vi.OrderId == ((orderId>0)?orderId:vi.OrderId)
                                select new {TravelerName= th.TravelerName, Quantity = vi.Qty}).Concat(
                                   from vi in dbContext.tblVirtualInventories join pr in dbContext.tblPurchaseRequisitions on vi.SrcId1 equals pr.PRid into vpr
                                       from x in vpr.DefaultIfEmpty() join po in dbContext.tblPurchaseOrders on x.POid equals po.POid into vprx
                                       from y in vprx.DefaultIfEmpty()  where vi.SrcType.ToLower() == "p" && vi.ItemId == itemId && vi.Qty > 0 
                                           select new {
                                               TravelerName = ((x.IsOutstanding == true)?SqlFunctions.StringConvert((decimal)x.PRid):y.POnumber),
                                               Quantity = vi.Qty
                                           }                                       
                                       ).ToList();

            foreach (var item in creditsQuery)
            {
                credits.Add(new Tuple<string, double?>(item.TravelerName, item.Quantity));
            }
            ViewBag.credits = credits;
            return PartialView();
        }

    }
}
