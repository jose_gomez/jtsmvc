﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;

namespace JTSMVC.Controllers
{
    public class OrdersOnHandController : Controller
    {
        public PartialViewResult OnHand(string ItemNumber)
        {
            ViewBag.Title = ItemNumber;
            return PartialView();
        }
    }
}
