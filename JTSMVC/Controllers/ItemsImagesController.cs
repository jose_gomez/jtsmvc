﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using JTSMVC.Models;

namespace JTSMVC.Controllers
{
    public class ItemsImagesController : ApiController
    {
        //
        // GET: /ItemsImages/

        public string GET(string itemNumber)
        {
            string path = "";
            try
            {
                path = PictureHelper.GetPictureURL(itemNumber);
            }
            catch (Exception ex)
            {

            }
            return path;
        }

    }
}
