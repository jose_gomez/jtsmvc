﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using JTSMVC.Models;

namespace JTSMVC.Controllers
{
    
    public class GetQuantityController : Controller
    {
        //
        // GET: /GetQuantity/
        public ActionResult Index(string ItemNumber)
        {
            Entities1 dbContext = new Entities1();
            string sql = @"SELECT tblItems.ItemNumber, tblItems.ItemID, tblItemBOMBRsteps.StepNo as StepNo, 
                             tblItems_1.ItemID AS ItemId2, tblItems_1.ItemNumber AS Item, 
                             0 AS [On Hand], 0 AS Needed, 0  Difference, 
                             0 AS [Total Needed],0  AS [Total Difference], tblVendors.VendorName, tblVendors.LeadTime as LeadTime 
                             FROM (tblItems INNER JOIN 
                             (((tblItemBOMBRsteps LEFT OUTER JOIN 
                             tblItemBOMBRstepItems ON tblItemBOMBRsteps.StepID = tblItemBOMBRstepItems.StepID) 
                             LEFT OUTER JOIN tblItems as tblItems_1 
                             ON tblItemBOMBRstepItems.ItemID = tblItems_1.ItemID) 
                             LEFT OUTER JOIN tblVendors ON tblItems_1.VendorId = tblVendors.VendorID) 
                             ON tblItems.ItemID = tblItemBOMBRsteps.ItemId) 
                             WHERE (tblItems_1.ItemNumber <> '') and tblItems.ItemNumber = @ItemNumber
                             ORDER BY tblItemBOMBRsteps.StepNo";
            SqlParameter param = new SqlParameter("@ItemNumber", ItemNumber);
            var query = dbContext.Database.SqlQuery<ItemQuantities>(sql, param);
            List<ItemQuantities> list = new List<ItemQuantities>();
            if (query != null)
            {
                foreach (var item in query)
                {
                    float containerQTY = 0;
                    float getVitQty  = 0;
                    float tGetVitQty = 0;
                    if (item.ItemId2 != null)
                    {
                        containerQTY = item.GetContainerQTY((int)item.ItemId2);
                        tGetVitQty = item.TgetVitQty((int)item.ItemId2);
                    }
                    if (item.ItemId != null && item.ItemId2 != null)
                    {
                        getVitQty = item.GetVITQty((int)item.ItemId2,(int)item.ItemId);
                    }
                    item.OnHand = (int)containerQTY;
                    item.Needed = (int)getVitQty;
                    item.Difference = (int)containerQTY - (int)getVitQty;
                    item.TotalNeeded = (int)tGetVitQty;
                    item.TotalDifference = (int)containerQTY - (int)tGetVitQty;
                    item.LeadTime = item.LeadTime;
                    list.Add(item);
                }
            }
           return View(list);
        }

    }

   
    public class ItemQuantities
    {
        Entities1 dbContext = new Entities1();
        public string ItemNumber { get; set; }
        public int? ItemId { get; set; }
        public int? StepNo { get; set; }
        public int? ItemId2 { get; set; }
        public string Item { get; set; }
        public int OnHand { get; set; }
        public int Needed { get; set; }
        public int Difference { get; set; }
        public int TotalNeeded { get; set; }
        public int TotalDifference { get; set; }
        public string VendorName { get; set; }
        public int? LeadTime { get; set; }
        public ItemQuantities()
        {

        }

        public ItemQuantities(string ItemNumber, int? ItemId, int? StepNo, int? ItemId2, string Item,
                            int OnHand, int Needed, int Difference, int TotalNeeded, int TotalDifference, string VendorName, int? LeadTime)
        {
            this.ItemNumber = ItemNumber;
            this.ItemId = ItemId;
            this.ItemId2 = ItemId2;
            this.Item = Item;
            this.OnHand = OnHand;
            this.Needed = Needed;
            this.Difference = Difference;
            this.TotalNeeded = TotalNeeded;
            this.TotalDifference = TotalDifference;
            this.VendorName = VendorName;
            this.LeadTime = LeadTime;
        }

        public float GetContainerQTY(int itemId)
        {
            string sql = @"SELECT SUM(tblContainers.Qty) AS qty 
                            FROM (tblLots LEFT OUTER JOIN (tblLocations RIGHT OUTER JOIN 
                            tblContainers ON tblLocations.LocationId = tblContainers.LocationId) 
                            ON tblLots.LotId = tblContainers.LotId) 
                            WHERE (tblLots.IsActive = 1) AND (tblContainers.IsEmpty = 0) AND 
                            (tblContainers.Qty > 0) AND (tblLots.ItemId = @ItemId)";
            SqlParameter param = new SqlParameter("@ItemId", itemId);
            var result = dbContext.Database.SqlQuery<double?>(sql, param).FirstOrDefault();
            float containerQty = 0;
            if (result != null && result.ToString() != "")
            {
                containerQty = (float)result;
            }
            return containerQty;
        }

        public float GetVITQty(int vItemId, int hItemId)
        {
            string sql = @"SELECT SUM(- tblVirtualInventory.Qty) AS Qty 
                            FROM (tblVirtualInventory INNER JOIN 
                            tblTravelerHeaders ON tblVirtualInventory.SrcId1 = tblTravelerHeaders.TravelerId) 
                            WHERE (tblVirtualInventory.ItemId = @ItemId) AND (tblVirtualInventory.SrcType = 'J') AND 
                            (tblTravelerHeaders.ItemID = @ItemId2) AND (- tblVirtualInventory.Qty > 0) ";
            SqlParameter param = new SqlParameter("@ItemId", vItemId);
            SqlParameter param2 = new SqlParameter("@ItemId2", hItemId);
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(param);
            paramList.Add(param2);
            var result = dbContext.Database.SqlQuery<double?>(sql, paramList.ToArray()).FirstOrDefault();
            float vitQty = 0;

            if (result != null && result.ToString() != "")
            {
                vitQty = (float)result;
            }
            return vitQty;
        }


        public float TgetVitQty(int vItem)
        {
            string sql = @"Select sum(-tblVirtualInventory.qty) as qty 
                            From tblVirtualInventory, tblTravelerHeaders 
                            Where -tblVirtualInventory.qty > 0 and 
                            travelerID = srcId1 and srcType = 'j' and 
                            tblVirtualInventory.ItemID = @ItemId";
            SqlParameter param = new SqlParameter("@ItemId", vItem);
            float tVitQty = 0;
            var result = dbContext.Database.SqlQuery<double?>(sql, param).FirstOrDefault();

            if (result != null && result.ToString() != "")
            {
                tVitQty = (float)result;
            }
            return tVitQty;
        }


    }
}
