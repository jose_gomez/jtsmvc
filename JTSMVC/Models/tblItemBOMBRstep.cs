//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace JTSMVC.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblItemBOMBRstep
    {
        public int StepID { get; set; }
        public Nullable<int> ItemId { get; set; }
        public Nullable<int> StepNo { get; set; }
        public Nullable<int> ActivityId { get; set; }
        public string Instructions { get; set; }
        public Nullable<bool> Revisado { get; set; }
    }
}
