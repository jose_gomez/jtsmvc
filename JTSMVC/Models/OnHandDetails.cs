﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JTSMVC.Models
{
    public class OnHandDetails
    {
        private double? defaultItemWeight;
        private string location;
        DateTime? orderDate;
        private string container;
        private string orderNo;
        private double? auxWeight;
        private double? containerPieceWeight;

        public double? ContainerPieceWeight
        {
            get { return containerPieceWeight; }
            set { containerPieceWeight = value; }
        }

        public double? DefaultItemWeight
        {
            get { return defaultItemWeight; }
            set { defaultItemWeight = value; }
        }

        public string Location
        {
            get { return location; }
            set { location = value; }
        }

        public DateTime? OrderDate
        {
            get { return orderDate; }
            set { orderDate = value; }
        }

        public string Container
        {
            get { return container; }
            set { container = value; }
        }


        public string OrderNo
        {
            get { return orderNo; }
            set { orderNo = value; }
        }

        public double? AuxWeight
        {
            get { return auxWeight; }
            set { auxWeight = value; }
        }
    }
}