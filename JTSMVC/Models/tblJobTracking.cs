//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace JTSMVC.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblJobTracking
    {
        public int EntryID { get; set; }
        public Nullable<int> TravelerID { get; set; }
        public Nullable<int> JobID { get; set; }
        public Nullable<int> StepId { get; set; }
        public Nullable<int> ActivityID { get; set; }
        public Nullable<int> InputID { get; set; }
        public Nullable<int> EntryType { get; set; }
        public Nullable<System.DateTime> EntryTime { get; set; }
        public Nullable<double> Qty { get; set; }
        public Nullable<double> AuxWeight { get; set; }
        public Nullable<int> EmployeeID { get; set; }
        public string Comments { get; set; }
        public Nullable<int> NextActivityID { get; set; }
        public Nullable<int> Reason { get; set; }
        public Nullable<bool> IsMatchingInOutPresent { get; set; }
        public Nullable<int> UOM { get; set; }
        public string Number { get; set; }
        public string Ubicacion { get; set; }
        public Nullable<int> SubActivity { get; set; }
        public string Proceso { get; set; }
    }
}
